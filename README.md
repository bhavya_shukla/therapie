# The Therapie Clinic Assignment

This Project helps the user to search availability at clinic from and to dates.
The design of the project is inspired by the layout given for the assignment.

[Tech] [covered]

Cloud Technology: Amazon S3
CI/CD: BitBucket Pipeline[(Developement)],[(Production)] and BitBucket Source Control
Front-end Tech: React JS
UI: React Bootstrap
API: Library Axios
Data Handler: React-redux
Styling: React-bootstrap , Node-Sass

## Enviornments for build

For the source code control we have used [BitBucket] . Pipeline have been build for developement and production, both the pipeline deploys it directly to Amazon S3. To verify you can go to the below mentioned links are the deployed pipeline

There are two enviornments 
[developement](https://therapie-dev-pipeline.s3.eu-west-1.amazonaws.com/index.html)

[production](https://therapie-prod-pipeline.s3.eu-west-1.amazonaws.com/index.html)

when the first commit and push is made to the master branch the Bitbucket pipeline creates a build and throws error if errors in the build process, else if successful, deploys directly to the  Amazon S3 bucket.
following is the process to commit and push

### `git add .`

### `git commit -a -m "Your Commit message"`

### `git push origin master`

## To Run BitBucket Pipeline

To implement your own pipeline you need to make following changes in [bitbucket-pipline.yaml]
need to use the package in bitbucket called [atlassian/aws-s3-deploy:0.3.8]
developement build is done on master branch and production build is deployed from master to production which is custom.
you need to configure following on your bitbucket to deploy pipeline
go to BitBucket Repository -> Repository Settings -> Deployments -> 
create developement and production variables and inside both add the following details

 - AWS_ACCESS_KEY_ID:  Your [$AWS_ACCESS_KEY_ID]
 - AWS_SECRET_ACCESS_KEY:Your [$AWS_SECRET_ACCESS_KEY]
 - AWS_DEFAULT_REGION: Your [$AWS_DEFAULT_REGION]
 - S3_BUCKET: Your [$S3_BUCKET]
 - ACL: "public-read"
 - LOCAL_PATH: build


After the deployment Successful to build for production 
Go to BitBucket repo 
Select [pipelines] ,
On the top you can see run pipeline ,
Click on [Run] [pipeline] ,
Select the [master] [branch] ,
Select the [custom[] [:] [production]  
Click [start]

This will build and deploy to the Amazon S3 bucket.

## Run Project Locally

In the project directory to test locally, you can run:

### `npm install`

Installs the project node modules

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Runs The Build.

## Project Requirement

Following are the Technologies used in developement process

[Tech] [Used]:
- ReactJs
- Redux
- BitBucket Source Control
- BitBucket Pipeline(For Developement and Production Automation Build and Push to AWS s3)
- Amazon S3 Developement Bucket
- Amazon S3 Production Bucket

[Library] [used]:
- React bootstrap
- React Redux
- Axios
- moment
- node-sass

## Project Structure
- bitbucket-pipelines.yaml
    - src
        - index.js
        - api
            - Baseapi.js
        - components (Listed According to the Parent Child hierarchy)
            - CustomerList.js( Implemented But Not Used)
                - CustomerComponent.js
            - App.js
                - Header.js
                - Filter.js
                    - SelectList.js
                    - DateInp.js
                        - SelectComponent.js
                - Slotscontainer.js
        - redux
            -Storedata.js
            - action
                - availableTimeActions.js
                - clinicActions.js
                - customerActions.js
                - serviceActions.js

            - constants
                - action-types.js
            - reducers
                - avalilableTimeReducer.js
                - clinicReducer.js
                - combineReducers.js
                - customerReducer.js
                - serviceReducer.js
                
        - styles(Node-sass auto generates css map and css Files)
            - Filter.scss
            - Header.scss
            - Slotescontainer.scss

## Redux Store Data 

Below mentioned about the reducers implemented

    allCustomers:customerReducer( implemented but not used)
    allClinics:clinicReducer,(get all Clinics)
    allServices:serviceReducer,(get all Services)
    allAvailableTime:availableTimeReducer,(get data on search available time)
    clinic:selectedClinicReducer,(get data on select available clinic)
    service:selectedServiceReducer,(get data on select available service)



    -------------------------------------------------------------------------------------------------------------