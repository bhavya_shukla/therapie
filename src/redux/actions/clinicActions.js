import { ActionTypes } from "../constants/action-types"
export const setClinics=(clinics)=>{
    return{
        type:ActionTypes.SET_CLINICS ,
        payload:clinics,
    };
};
export const selectedClinic=(clinic)=>{
    return{
        type:ActionTypes.SELECTED_CLINIC ,
        payload:clinic,
    };
};