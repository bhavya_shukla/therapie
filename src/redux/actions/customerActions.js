import { ActionTypes } from "../constants/action-types"
export const setCustomers=(customers)=>{
    return{
        type:ActionTypes.SET_CUSTOMERS ,
        payload:customers,
    };
};
export const selectedCustomer=(customer)=>{
    return{
        type:ActionTypes.SELECTED_CUSTOMER ,
        payload:customer,
    };
};