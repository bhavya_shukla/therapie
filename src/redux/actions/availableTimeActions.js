import { ActionTypes } from "../constants/action-types"
export const setAvaliableTime=(availableTime)=>{
    return{
        type:ActionTypes.AVAILABLE_TIMES ,
        payload:availableTime,
    };
};