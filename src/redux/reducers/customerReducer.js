import { ActionTypes } from "../constants/action-types";

const initialState = {
  customers: [],
};
export const customerReducer = (state= initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_CUSTOMERS:
      return {...state,customers : payload};
    default:
      return state;
  }
};
