import {combineReducers} from "redux";
import {customerReducer} from "./customerReducer";
import { clinicReducer , selectedClinicReducer} from "./clinicReducer";
import { serviceReducer,selectedServiceReducer } from "./serviceReducer";
import { availableTimeReducer } from "./availableTimeReducer";
const reducers=combineReducers({
    allCustomers:customerReducer,
    allClinics:clinicReducer,
    allServices:serviceReducer,
    allAvailableTime:availableTimeReducer,
    clinic:selectedClinicReducer,
    service:selectedServiceReducer,
});
export default reducers;