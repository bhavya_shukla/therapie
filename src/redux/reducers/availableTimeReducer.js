import { ActionTypes } from "../constants/action-types";

const initialState = {
  availableTimes: [],

};
export const availableTimeReducer= (state= initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.AVAILABLE_TIMES:
      return {...state,availableTimes : payload};
    default:
      return state;
  }
};