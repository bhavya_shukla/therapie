import { ActionTypes } from "../constants/action-types";

const initialState = {
  clinics: [],

};
export const clinicReducer = (state= initialState, { type, payload }) => {
  switch (type) {
    case ActionTypes.SET_CLINICS:
      return {...state,clinics : payload};
    default:
      return state;
  }
};

export const selectedClinicReducer=(state={},{type,payload})=>{
 
  switch(type){
   
    case ActionTypes.SELECTED_CLINIC:
       return {...state,...payload};
    default:
      return state;
  }
};