import React from "react";
import { Card,ListGroup,ListGroupItem,Row, Col } from "react-bootstrap";
import { useSelector } from "react-redux";


const CustomerComponent = () => {
  const customers = useSelector((state )=> state.allCustomers.customers);
  
  const renderCustomer = customers.map((customer)=>{
    const { customerId, firstName, lastName, mobile } = customer;
    
    return (
       
          
               <Col key={customerId} className="m-2">
          <Card style={{ width: "18rem" }}>
        
            <Card.Body>
              <Card.Title>Customer</Card.Title>
              
            </Card.Body>
            <ListGroup className="list-group-flush">
              <ListGroupItem>{firstName} {lastName}</ListGroupItem>
              <ListGroupItem>{mobile}</ListGroupItem>
              <ListGroupItem>Vestibulum at eros</ListGroupItem>
            </ListGroup>
           
          </Card>
          </Col>
          
     
    );
});

  return <>
  <Row>
  
  {renderCustomer}
  
  </Row>
  
  
  </>;
};

export default CustomerComponent;
