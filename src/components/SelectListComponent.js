import React from 'react'


import {Form} from "react-bootstrap"


const SelectListComponent = ({valueb,setValueb,dataDisplay,title}) => {
  

    

    return (<>
   
   
   <Form.Select  size="sm"  onChange={e=>setValueb(e.target.value)} value={valueb}>
   <option  value="">{title}</option>
    {dataDisplay}
    
    </Form.Select></>
    )
}

export default SelectListComponent;
