import React from "react";
import Header from "./Header";
import Filter from "./Filter";
import api from "../api/Baseapi";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import Slotscontainer from "./Slotscontainer";
import {  Col,Badge,Row } from "react-bootstrap";
import { setAvaliableTime } from "../redux/actions/availableTimeActions";
import moment from "moment";

function App() {

  const [valuea, setValue] = useState("");
  const [selectedVal, setselectedVal] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [showResults, setShowResults] = useState(false)

  const sd = moment(startDate).format("yyyy-MM-DD");

  const ed = moment(endDate).format("yyyy-MM-DD");

  const getDatesDiff = (start_date, end_date, date_format = "YYYY-MM-DD") => {
    const getDateAsArray = (date) => {
      return moment(date.split(/\D+/), date_format);
    };
    const diff =
      getDateAsArray(end_date).diff(getDateAsArray(start_date), "days") + 1;
    const dates = [];
    for (let i = 0; i < diff; i++) {
      const nextDate = getDateAsArray(start_date).add(i, "day");
      const isWeekEndDay = nextDate.isoWeekday() > 5;
      if (!isWeekEndDay) dates.push(nextDate.format(date_format));
    }
    return dates;
  };

  const LOCAL_STORAGE_KEY = "customers";
  const [customers, setCustomers] = useState([]);
  const date_log = getDatesDiff(sd, ed);
  const available = useSelector(
    (state) => state.allAvailableTime.availableTimes
  );

 
  const dispatch = useDispatch();
   const renderat= available.map((item,i) => {
    const abt=item.data.map(date=>{
      return   <Col  className="text-center align-self-center p-2" >
       <div>{date.startTime.slice(1,3)}:{date.endTime.slice(4,6)}</div> 
      </Col>
     })
    return <><Col className="m-2">{date_log[i]}</Col><Row>{abt}</Row></>
       
     
  })

     

        
            //  return item.data.map(date=>{
            //    return <Col lg={3} md={6} sm={12} xs={12} className="text-center p-2">
            //     <div><Badge className="badge">{date_log[i]}</Badge> <Badge bg="success">{date.startTime.slice(1,3)}:{date.endTime.slice(4,6)}</Badge></div> 
            //    </Col>
            //   })

  // const renderat=date_log.map((date) => {
  //   return (
  //     <div>{date}
  //     <div>{ab}</div>
      
  //     </div>
        
  //   );
  // });
  

//   const renderat = date_log.map((date) => {

//     return <div><Badge className="badge">{date}</Badge></div> 
//     (available.map((date) => {
      
//       <Col lg={3} md={6} sm={12} xs={12} className="text-center p-2">
//         <div>
//           {" "}
//           <Badge bg="success">
//             {date.data.startTime}:{date.data.endTime}
//           </Badge>
//         </div>
//       </Col>;
//     }));
    
    
// })
  
  // available.map((item,i) => {
  // //   <div>
  // //   {date_log.map((day) => {
  // //     <div>{day}</div>;
  // //     {
  // //       available.map((date) => {
  // //         <Col lg={3} md={6} sm={12} xs={12} className="text-center p-2">
  // //           <div>
  // //             {" "}
  // //             <Badge bg="success">
  // //               {date.data.startTime.slice(1, 3)}:{date.data.endTime.slice(4, 6)}
  // //             </Badge>
  // //           </div>
  // //         </Col>;
  // //       });
  // //     }
  // //   })}
  // // </div>

  // return available.map((date) => {
  //   return <div><Badge className="badge">{date}</Badge></div>
  // })

     

        
  //           //  return item.data.map(date=>{
  //           //    return <Col lg={3} md={6} sm={12} xs={12} className="text-center p-2">
  //           //     <div><Badge className="badge">{date_log[i]}</Badge> <Badge bg="success">{date.startTime.slice(1,3)}:{date.endTime.slice(4,6)}</Badge></div> 
  //           //    </Col>
  //           //   })
        
        
   
    

  // });


 

  const getAppointment = (valuea, selectedVal, date) => {
    return api.get(`/clinics/${valuea}/services/${selectedVal}/timeslots/${date}`)
  };

 

  const datafetch = async () => {
    setShowResults(true);
    let actualdata = [];
   
    for (let i = 0; i < date_log.length; i++) {
      actualdata.push(getAppointment(valuea, selectedVal, date_log[i]));
    }
    Promise.all(actualdata).then(function (values) {
      dispatch(setAvaliableTime(values))
  
    
    });
    
 
   
  };

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(customers));
  }, [customers]);

  

  return (
    <>
      <Header />
      <Filter
        valuea={valuea}
        setValue={setValue}
        customers={customers}
        setCustomer={setCustomers}
        selectedVal={selectedVal}
        setselectedVal={setselectedVal}
        startDate={startDate}
        setStartDate={setStartDate}
        endDate={endDate}
        setEndDate={setEndDate}
        dataclick={datafetch}
        showResults={showResults}
        setShowResults={setShowResults}
      />
      <Slotscontainer renderat={renderat} showResults={showResults}setShowResults={setShowResults}/>
    </>
  );
}

export default App;
