import { Container,Card,Row, Col} from "react-bootstrap"
import "../styles/Slotscontainer.css";

const Slotscontainer = ({renderat,setShowResults,showResults}) => {
  console.log(showResults);
   
    return (
        <div>
           {showResults?<Container className="slots__container">
              <Row>
                  <Col lg={{span:8, offset:2}}>
                  <div><b>Available Times</b></div>
                  {renderat}
            
                  </Col>
              </Row>
            </Container>:<></>}
            
        </div> 
    )
}

export default Slotscontainer
