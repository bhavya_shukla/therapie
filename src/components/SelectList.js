import React,{useEffect} from 'react'
import { Row,Col } from 'react-bootstrap'
import {useSelector,useDispatch} from 'react-redux'
import SelectListComponent from './SelectListComponent';
import api from "../api/Baseapi";
import { setServices ,selectedService} from "../redux/actions/serviceActions";
import { selectedClinic, setClinics } from "../redux/actions/clinicActions";






const SelectList = ({selectedVal,setselectedVal,valuea, setValue}) => {

/*Service Data */

const ct="Clinics";
const st="Services";
const services = useSelector((state )=> state.allServices.services);


const retrieveServices = async () => {
  const response = await api.get("/services").catch((err) => {
    console.log("Error", err);
  });
  dispatch(setServices(response.data));
};

const fetchSelectedService= async () => {
    if(selectedVal && selectedVal !==""){
        const response = await api.get(`/services/${selectedVal}`).catch((err) => {
            console.log("Error", err);
        });
        dispatch(selectedService(response.data));
      
}
  
};

useEffect(() => {
  const getALLServices = async () => {
    const allServices = await retrieveServices();
  };
  getALLServices();
}, []);

useEffect(() => {
  if(selectedVal && selectedVal!=="" )fetchSelectedService();
}, [selectedVal])


const renderService = services.map((service)=>{
    const { serviceId, name, price, durationInMinutes } = service;
       
      return (   
          <option value={serviceId} key={serviceId} >{name} : {price}</option>    
    );
  });







/*Clinic Data */
const clinics = useSelector((state )=> state.allClinics.clinics);

const dispatch = useDispatch();


const retrieveClinics = async () => {
    const response = await api.get("/clinics").catch((err) => {
      console.log("Error", err);
    });
    dispatch(setClinics(response.data));
  
  };
  const fetchSelectedClinic = async () => {
    if(valuea && valuea !==""){
        const response = await api.get(`/clinics/${valuea}`).catch((err) => {
            console.log("Error", err);
            });
            dispatch(selectedClinic(response.data));
      
          
    }

        
  };

  useEffect(() => {
    const getALLClinics = async () => {
      const allClinics = await retrieveClinics();
    };
    getALLClinics();
 
  }, []);

useEffect(() => {   
  if(valuea && valuea!=="" )fetchSelectedClinic();
}, [valuea])


const renderClinic = clinics.map((clinic)=>{
 
    const { clinicId, country, streetAddress, geoLocation } = clinic;
      return (
          <option value={clinicId}  key={clinicId}>{streetAddress},{country}</option>
    );
  });

    return (
        <div>
             <Row className="m-1 ">
            <Col sm={12} md={6}  lg={6} className="my-2">
              <SelectListComponent valueb={valuea} setValueb={setValue} dataDisplay={renderClinic} title={ct}/>
            </Col>
            <Col sm={12} md={6}  lg={6} className="my-2">
              <SelectListComponent valueb={selectedVal} setValueb={setselectedVal} dataDisplay={renderService} title={st}/>
            </Col>
            </Row>
        </div>
    )
}

export default SelectList
