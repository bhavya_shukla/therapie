import React from "react";
import PropTypes from "prop-types";
import { Navbar, Container } from "react-bootstrap";
import "../styles/Header.css";

const Header = ({ title }) => {
  return (
    <Navbar className="color-nav">
      <Container>
        <Navbar.Brand href="#home" className="mx-auto nav-brand">
          {title}
        </Navbar.Brand>
      </Container>
    </Navbar>
  );
};
Header.defaultProps = {
  title: "Therapie Availability Search",
};
Header.propTypes = {
  title: PropTypes.string,
};
export default Header;
