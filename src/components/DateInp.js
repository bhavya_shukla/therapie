
import { Form } from "react-bootstrap"
import PropTypes from 'prop-types'

const DateInp = ({title,valDate,setDate}) => {
  
 
  return (
        <>
             <Form.Group controlId="dob">
              
              <Form.Control type="date" name="dob" placeholder={title} value={valDate} onChange={e=>setDate(e.target.value)}/>

            </Form.Group>
        </>
    )
}

DateInp.defaultProps = {
    title: "Date",
  };
  DateInp.propTypes = {
    title: PropTypes.string,
  };

export default DateInp
