import React from 'react'
import { useDispatch,useSelector } from 'react-redux';
import api from "../api/Baseapi";
import { setCustomers } from '../redux/actions/customerActions';
import {useEffect } from "react";
import CustomerComponent from './CustomerComponent';
import { Container } from 'react-bootstrap';


const CustomersList = () => {
    const customers = useSelector((state)=> state.allCustomers.customers);
   
   const dispatch = useDispatch();

    const retrieveCustomers= async()=>{
        const response = await api.get("/customers").catch((err)=>{
          console.log("Error",err);
        });
        dispatch(setCustomers(response.data));
      };
     useEffect(() => {
      const getALLCustomers=async ()=>{
       const allCustomers=await retrieveCustomers();
      };
      getALLCustomers();
     }, []);

    return (
        <div>
           
            <Container>
            <h4>Customer List</h4>
            <CustomerComponent/>
            </Container>
        </div>
    )
}

export default CustomersList
