import PropTypes from "prop-types";
import { Container, Card, Row, Col, Button } from "react-bootstrap";
import "../styles/Filter.scss";
import SelectList from "./SelectList";

import DateInp from "./DateInp";

const Filter = ({
  title,
  selectedVal,
  setselectedVal,
  valuea,
  setValue,
  startDate,
  setStartDate,
  endDate,
  setEndDate,
  dataclick,
  setShowResults,
  showResults,
}) => {
  const onClick = () => {
    if (
      selectedVal !== "" &&
      valuea !== "" &&
      startDate !== "" &&
      endDate !== ""
    ) {
      dataclick();
    }
  };

  return (
    <div>
      <Container className="filterwrapper" >
        
        <Row >
        
          <Col lg={{span:6, offset:3}} className="filter_wrapper_inner">
          <Col >
        <b>{title}</b>
        </Col>

          <SelectList
            selectedVal={selectedVal}
            setselectedVal={setselectedVal}
            valuea={valuea}
            setValue={setValue}
           
          />

          <Row className="m-1 ">
            <Col className="my-1" sm={12} md={6} lg={6}>
              <DateInp
                title="From Date"
                valDate={startDate}
                setDate={setStartDate}
              />
            </Col>
            <Col className="my-1" sm={12} md={6} lg={6}>
              <DateInp title="to Date" valDate={endDate} setDate={setEndDate} />
            </Col>

            <Col
              lg={{ span: 3, offset: 9 }}
              xl={{ span: 3, offset: 9 }}
              md={{ span: 12 }}
              className="my-3"
            >
              <Button
                variant="info"
                size="sm"
                onClick={onClick}
                className="w-100"
              >
                Search
              </Button>
            </Col>
          </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
Filter.defaultProps = {
  title: "Filter",
};
Filter.propTypes = {
  title: PropTypes.string,
};
export default Filter;
